# Sudo Show 43 - Back to School Part 1

[Episode 43](https://sudo.show/43)


## Show Notes

Brandon and Bill have a conversation about the education space and the current state of technology in education.  We get into the current solutions and who the decision makers are in this space.


### Links:
[Destination Linux Network](https://destinationlinux.network)
[Sudo Show Website](https://sudo.show)
[Sponsor: Bitwarden](https://bitwarden.com/dln)
[Sponsor: Digital Ocean](https://do.co/dln)

[K12 Support](https://k12irc.org/tools/computers/linux.php)

### Support:
[Sudo Show Patreon](https://sudo.show/patreon)
[Sudo Show Sponsus](https://sudo.show/sponsus)
[Sudo Show Swag](https://sudo.show/swag)

### Contact Us:
[DLN Discourse](https://sudo.show/discuss)
[Email Us!](mailto:contact@sudo.show)
[Sudo Matrix Room](https://sudo.show/matrix)

### Follow our Hosts:
[Brandon's Website](https://open-tech.net)


## Chapters
- 00:00  Intro
- 00:42  Welcome
- 01:37  Digitial Ocean AD
- 02:36  Bitwarden Ad
- 03:40  Main Content
- 32:10  Outro
